import os
import sqlite3
import json
from dotenv import load_dotenv
import configparser

# Set up Config
config = configparser.ConfigParser()
config.read('sierra.ini')

DB_PATH = os.path.join(os.path.dirname(__file__), config.get('Database', 'Filename'))

# Startup and Setup stuff

def ConnectDB():
	dbconnection = sqlite3.connect(DB_PATH)
	return dbconnection

def GetCursor():
	cursor = ConnectDB().cursor()
	return cursor

def SetupDB():
	cur = GetCursor()
	
	roles_sql = """
	CREATE TABLE IF NOT EXISTS roles (
	role_id text PRIMARY KEY,
	role text NOT NULL,
	guild text NOT NULL,
	is_leadership int NOT NULL)"""
	cur.execute(roles_sql)

	users_sql = """
	CREATE TABLE IF NOT EXISTS users (
	database_id text PRIMARY KEY,
	name text NOT NULL,
	id text NOT NULL,
	roles text NOT NULL,
	guild text NOT NULL,
	date text NOT NULL)"""
	cur.execute(users_sql)

def IDGen(name, guild):
	returnID = str(name) + " - " + str(guild)
	return returnID

def PurgeGuild(guild):
	con = ConnectDB()
	cur = con.cursor()
	cur.execute('DELETE FROM users WHERE guild=?', (guild,))
	cur.execute('DELETE FROM roles WHERE guild=?', (guild,))
	con.commit()

def AddUser(memberName, memberID, memberRoles, guild, date):
	connection = ConnectDB()
	cursor = connection.cursor()
	command_string = 'INSERT OR REPLACE INTO users(database_id, name, id, roles, guild, date) VALUES (?,?,?,?,?,?)'
	cursor.execute(command_string, (IDGen(memberID, guild), memberName, memberID, json.dumps(memberRoles), guild, date))
	connection.commit()

def GetUser(memberID, guild):
	cur = GetCursor()
	foundUser = cur.execute('SELECT * FROM users WHERE database_id=? ORDER BY date', (IDGen(memberID, guild),)).fetchone()
	return foundUser

def RemoveUser(memberID, guild):
	connection = ConnectDB()
	cursor = connection.cursor()
	cursor.execute('DELETE FROM users WHERE database_id=?', (IDGen(memberID, guild),))
	connection.commit()

def AddLeadershipRole(roleName, guild):
	connection = ConnectDB()
	cursor = connection.cursor()
	command_string = 'INSERT OR REPLACE INTO roles(role_id, role, guild, is_leadership) VALUES (?,?,?,?)'
	cursor.execute(command_string, (IDGen(roleName, guild), roleName, guild, 1))
	connection.commit()

def RemoveRole(roleName, guild):
	connection = ConnectDB()
	cursor = connection.cursor()
	cursor.execute('DELETE FROM roles WHERE role_id=?', (IDGen(roleName, guild),))
	connection.commit()

def AddOffDutyRole(roleName, guild):
	connection = ConnectDB()
	cursor = connection.cursor()
	command_string = 'INSERT OR REPLACE INTO roles(role_id, role, guild, is_leadership) VALUES (?,?,?,?)'
	cursor.execute(command_string, (IDGen(roleName, guild), roleName, guild, 0))
	connection.commit()

def OnStartup():
	print('Setting up Database.')
	SetupDB()

def ListUsers(guild):
	cur = GetCursor()
	allUsers = cur.execute('SELECT * FROM users WHERE guild=?', (guild,))
	return allUsers

def GetUserCount(guild):
	cur = GetCursor()
	cur.execute("SELECT * FROM users WHERE guild=?", (guild,))
	return len(cur.fetchall())

def GetRoleCount(guild):
	cur = GetCursor()
	cur.execute("SELECT * FROM roles WHERE guild=?", (guild,))
	return len(cur.fetchall())

def GetOffDutyRole(guild):
	cur = GetCursor()
	roles = cur.execute("SELECT * FROM roles WHERE guild=? AND is_leadership=0", (guild,)).fetchone()
	return roles

def GetAllRoles(guild):
	cur = GetCursor()
	allRoles = cur.execute('SELECT * FROM roles WHERE guild=?', (guild,))
	return allRoles

def GetLeadershipRoles(guild):
	cur = GetCursor()
	allRoles = cur.execute('SELECT * FROM roles WHERE guild=? AND is_leadership=1', (guild,))
	return allRoles