import os
import configparser
import discord
from dotenv import load_dotenv
from discord.ext import commands
import db_tools

# TODO Create parser function for DISCORD_PREFIX, to allow for multiple prefixes
# TODO Add usage (and aliases?) to various commands?
# TODO Change help to use usage rather than name

# To use fetch_members(), we need to set the members intent.
intents = discord.Intents.default()

# Set up Config
config = configparser.ConfigParser()
config.read('sierra.ini')

# Load Environment Variables and other properties
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
BOT_VERSION = config.get('Bot', 'Version')
BOT_REPO = config.get('Bot', 'Repo')
DISCORD_COLOR = int(config.get('Discord', 'Color'), 16)
DISCORD_PREFIX = config.get('Discord', 'Prefix')

# Set prefix
def GetPrefix(bot, message):
	prefixes=[DISCORD_PREFIX]
	return commands.when_mentioned_or(*prefixes)(bot, message)

bot=commands.Bot(command_prefix=GetPrefix, intents=intents)

# Remove default Help command so we can use our own instead
bot.remove_command('help')
initial_extensions = [
	'cogs.core',
	'cogs.help',
]

# When the bot is logged in and ready, let me know in the console
@bot.event
async def on_ready():
	await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=f'for \'od;\' mentions. | OffDutyBot ver. {BOT_VERSION}'))
	for extension in initial_extensions:
		bot.load_extension(extension)
	print(f'\nLogged in as: {bot.user.name} - {bot.user.id}\nVersion: {BOT_VERSION}\n')
	print(f'Looks like we\'re ready!')

# Just a brief greeting command
@bot.command(name='reload',
			 usage='reload',
			 hidden=True,
			 help='Reloads all modules',
			 brief='Reloads all modules.')
@commands.has_permissions(administrator=True)
async def Reload(ctx):
	try:
		for extension in initial_extensions:
			bot.reload_extension(extension)
		await ctx.send('Reloaded all my modules!')
	except:
		# If something fails, let user know which extension broke.
		if extension:
			await ctx.send(f'Hm. I can\'t seem to reload {extension} right now.')
			raise
		else:
			await ctx.send(f'Hm. I\'m having problems reloading things. If you can, check the console.')
			raise

@bot.command(name='ping',
			 usage='ping',
			 brief='Pings me!',
			 help='Pings me!')
async def Ping(ctx):
	await ctx.send('Hiya!')

db_tools.OnStartup()
print('Logging into Discord.')
bot.run(TOKEN)