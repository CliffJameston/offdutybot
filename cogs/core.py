import discord
import configparser
from discord.ext import commands
from discord.errors import Forbidden
import db_tools
import json
from datetime import date

# Set up Config
config = configparser.ConfigParser()
config.read('sierra.ini')

BOT_NOT_IMPLEMENTED = config.get('Bot', 'Not Implemented')
DISCORD_COLOR = int(config.get('Discord', 'Color'), 16)

async def SendEmbed(ctx, embed):
	"""
	Function that handles the sending of embeds
	- Takes context and embed to send
	- tries to send embed in channel
	- tries to send normal message when that fails
	- tries to send embed private with information about missing permissions
	"""
	try:
		await ctx.send(embed=embed)
	except Forbidden:
		try:
			await ctx.send("Hey, seems like I can't send embeds. Please check my permissions :)")
		except Forbidden:
			await ctx.author.send(
				f"Hey, seems like I can't send any messages in {ctx.channel.name} on {ctx.guild.name}\n"
				f"You should probably let the server staff know about this. :slight_smile: ", embed=embed)

def IsServerSetup(ctx):
	if db_tools.GetLeadershipRoles(ctx.guild.name) is None:
		return False
	if db_tools.GetOffDutyRole(ctx.guild.name) is None:
		return False
	return True

async def FindServerRole(ctx, roleToFind):
	# Find the role provided. We support using a mention of the role, the text name of the role, or the role ID,
	# so try each one in order
	ReturnRole = discord.utils.get(ctx.guild.roles, mention=roleToFind)
	if ReturnRole is not None:
		return ReturnRole
	ReturnRole = discord.utils.get(ctx.guild.roles, name=roleToFind)
	if ReturnRole is not None:
		return ReturnRole
	try:
		ReturnRole = ctx.guild.get_role(int(roleToFind))
		if ReturnRole is not None:
			return ReturnRole
	except ValueError:
		raise LookupError(f'I couldn\'t find `{roleToFind}`, are you sure that\'s right?')
	raise RuntimeError(f'Somehow, you reached the last line of FindServerRole(). That shouldn\'t be able to happen.')

async def FindServerMember(ctx, memberToFind):
	# Find the role provided. We support using a mention of the role, the text name of the role, or the role ID,
	# so try each one in order
	ReturnMember = discord.utils.get(ctx.guild.members, mention=memberToFind)
	if ReturnMember is not None:
		return ReturnMember
	ReturnMember = discord.utils.get(ctx.guild.members, name=memberToFind)
	if ReturnMember is not None:
		return ReturnMember
	try:
		ReturnMember = ctx.guild.get_member(int(memberToFind))
		if ReturnMember is not None:
			return ReturnMember
	except ValueError:
		raise LookupError(f'I couldn\'t find `{memberToFind}`, are you sure that\'s right?')
	raise RuntimeError(f'Somehow, you reached the last line of FindServerMember(). That shouldn\'t be able to happen.')

class Core(commands.Cog):#, description='All the main functionality.'):

	def __init__(self, bot):
		self.bot = bot

	@commands.command(name='off-duty',
                      usage='off-duty',
					  brief='Marks you as off-duty, and removes leadership roles',
					  help='Adds the user who ran the command to the database of off-duty users along with their '
						   'current roles, grants them the @off-duty role, and then removes any of their roles that '
						   'appear on the list of leadership roles.')
	async def SetUserOffDuty(self, ctx):
		# Check if the server is fully set up first
		if IsServerSetup(ctx) is False:
			await ctx.send(f'It seems like this server isn\'t quite set up yet. Make sure to run `setRole` and `addOffDuty`')
			return

		# Check if a user is already listed as off-duty
		dbRecord = db_tools.GetUser(ctx.author.id, ctx.guild.name)
		if dbRecord is not None:
			# Error: Found user in Off-Duty list
			await ctx.send(f'Sorry, I have you listed as off-duty right now.')
			return

		# Create list of user's leadership roles to save to database
		roles = ctx.author.roles
		userLeadershipRoles = []
		dbRoles = db_tools.GetLeadershipRoles(ctx.guild.name)
		leadershipRoles = []
		for each in dbRoles:
			leadershipRoles.append(each[1])
		for role in roles:
			if role.name in leadershipRoles:
				userLeadershipRoles.append(role.name)

		# Add @off-duty Role to User
		offDutyRole = await FindServerRole(ctx, db_tools.GetOffDutyRole(ctx.guild.name)[1])
		await ctx.author.add_roles(offDutyRole)

		# Remove Leadership Roles from User
		for role in userLeadershipRoles:
			roleToRemove = await FindServerRole(ctx, role)
			await ctx.author.remove_roles(roleToRemove)

		# Save User to database
		db_tools.AddUser(ctx.author.name, ctx.author.id, userLeadershipRoles, ctx.guild.name, date.today().strftime('%Y-%m-%d'))

		# Send confirmation that everything happened
		await ctx.send(f'Successfully set you off-duty. Enjoy your break!')

	@commands.command(name='on-duty',
                      usage='on-duty',
					  brief='Returns your leadership roles',
					  help='Checks the database for the user who ran the command. If they\'re listed, add back any '
						   'leadership roles they are missing, then remove the @off-duty role.')
	async def SetUserOnDuty(self, ctx):
		# Check if the server is fully set up first
		if IsServerSetup(ctx) is False:
			await ctx.send(f'It seems like this server isn\'t quite set up yet. Make sure to run `setRole` and `addOffDuty`')
			return

		# Find User in database
		dbRecord = db_tools.GetUser(ctx.author.id, ctx.guild.name)
		if dbRecord is None:
			# Error: Could not find user in Off-Duty list
			await ctx.send(f'Sorry, I don\'t have you listed as off-duty right now.')
			return

		# Add saved Leadership Roles to User
		roles = json.loads(dbRecord[3])
		dbRoles = db_tools.GetLeadershipRoles(ctx.guild.name)
		leadershipRoles = []
		for each in dbRoles:
			leadershipRoles.append(each[1])
		for role in roles:
			if role in leadershipRoles:
				roleToAdd = await FindServerRole(ctx, role)
				await ctx.author.add_roles(roleToAdd)

		# Remove @off-duty Role from User
		offDutyRole = await FindServerRole(ctx, db_tools.GetOffDutyRole(ctx.guild.name)[1])
		await ctx.author.remove_roles(offDutyRole)

		# Remove User from database
		db_tools.RemoveUser(ctx.author.id, ctx.guild.name)

		# Send confirmation that everything happened
		await ctx.send(f'Successfully set you back on-duty. Welcome back!')

	@commands.command(name='remove',
                      usage='remove <user>',
					  brief='Removes any saved info for the given user (Admin Only)',
					  help='Checks the database for the user provided. If they\'re listed, completely purges their '
						   'entry from the list. This includes all their saved roles, so make sure you have their roles'
						   ' noted somewhere else if you need to restore them in the future.')
	@commands.has_permissions(administrator=True)
	async def RemoveUserRecord(self, ctx, user):
		# Check if the server is fully set up first
		if IsServerSetup(ctx) is False:
			await ctx.send(f'It seems like this server isn\'t quite set up yet. Make sure to run `setRole` and `addOffDuty`')
			return

		# Find user in server
		try:
			userToRemove = FindServerMember(user)
		except LookupError as error:
			await ctx.send(format(error))
			return

		# Find User in database
		dbRecord = db_tools.GetUser(userToRemove.id, ctx.guild.name)
		if dbRecord is None:
			# Error: Could not find user in Off-Duty list
			await ctx.send(f'Sorry, I don\'t have {userToRemove.name} in my database.')
			return

		# Remove User from database
		db_tools.RemoveUser(userToRemove.id, ctx.guild.name)

		# Send confirmation that everything happened
		await ctx.send(f'Successfully deleted {userToRemove.name}.')

	@commands.command(name='addRole',
                      usage='addRole <role>',
					  brief='Add leadership role(s) to add/remove from users (Admin Only)',
					  help='Adds the provided role(s) to the list of roles I\'ll add or remove when setting someone '
						   'on- or off-duty. This requires either the full name, the ID, or the @mention of the role, '
						   'but won\'t find the role from just the name correctly if it has a space.')
	@commands.has_permissions(administrator=True)
	async def AddLeadershipRole(self, ctx, *roleToFind):
		replyText = 'Status:\n'
		# Find the roles provided
		for arg in roleToFind:
			try:
				role = await FindServerRole(ctx, arg)
				db_tools.AddLeadershipRole(role.name, ctx.guild.name)
				replyText += f'{role.name} - Added\n'
			except LookupError:
				replyText += f'{arg} - Error\n'
		# Add the role to the database
		# Send confirmation that roles were added, and print any roles that couldn't be found
		await ctx.send(replyText)

	@commands.command(name='setOffDuty',
                      usage='setOffDuty <role>',
					  brief='Set the server\'s Off-Duty role (Admin Only)',
					  help='Sets the role I\'ll add when a user goes off-duty. This requires either the full name, the '
						   'ID, or the @mention of the role, but won\'t find the role from just the name correctly '
						   'if it has a space.')
	@commands.has_permissions(administrator=True)
	async def AddOffDutyRole(self, ctx, roleToFind):
		# Find the role provided
		try:
			role = await FindServerRole(ctx, roleToFind)
			# Add the role to the database
			db_tools.AddOffDutyRole(role.name, ctx.guild.name)
			# Send confirmation that role was added
			await ctx.send(f'Successfully set {role.name} as an Off-Duty role')
		except LookupError as error:
			# Print if the role could not be found
			await ctx.send(format(error))

	@commands.command(name='listRoles',
                      usage='listRoles',
					  brief='List all this server\'s leadership roles',
					  help='Lists all the roles (That you\'ve told me about) that you have determined are "Leadership" '
						   'roles. These are the *only* roles I will remove from users.')
	async def ListLeadershipRoles(self, ctx):
		# Get list of roles from database
		list = db_tools.GetAllRoles(ctx.guild.name)
		sendstring = 'Server Roles:\n'
		for role in list:
			if role[3] == 1:
				sendstring += f'{role[1]} - Leadership\n'
			else:
				sendstring += f'{role[1]} - Off-Duty\n'
		# Send message listing all roles
		await ctx.send(sendstring)

	@commands.command(name='removeRole',
                      usage='removeRole <roles>',
					  brief='Remove leadership role(s) from the list (Admin Only)',
					  help='Removes the provided role(s) from the list of roles I\'ll add or remove when setting someone '
						   'on- or off-duty.')
	@commands.has_permissions(administrator=True)
	async def RemoveLeadershipRole(self, ctx, *roleToFind):
		replyText = 'Status:\n'
		# Find the roles provided
		for arg in roleToFind:
			try:
				role = await FindServerRole(ctx, arg)
				# Remove roles from database
				db_tools.RemoveRole(role.name, ctx.guild.name)
				replyText += f'{role.name} - Removed\n'
			except LookupError:
				replyText += f'{arg} - Error\n'
		# Send confirmation that roles were removed, and print any that couldn't be found/removed
		await ctx.send(replyText)

	@commands.command(name='purgeServer',
                      usage='purgeServer',
					  brief='Clears all saved users for the current server (Admin Only)',
					  help='This command clears *all* information I have stored for this server. This includes all '
						   'currently off-duty users, the roles I have saved for them, and even the roles you have '
						   'told me are Leadership roles. If you are planning on removing me from the server, run this '
						   'command beforehand, but it\'s otherwise almost *certainly* not what you\'re looking for.')
	@commands.has_permissions(administrator=True)
	async def PurgeServerRecords(self, ctx):
		# TODO - Make Admin only
		# TODO - Ask for confirmation
		db_tools.PurgeGuild(ctx.guild.name)
		startMessage = discord.Embed(title='Server Purged',
									 description=f'Successfully removed all entries for the server "{ctx.guild.name}" from the database.',
									 color=DISCORD_COLOR)
		await SendEmbed(ctx, startMessage)

	@commands.command(name='listUsers',
                      usage='listUsers',
					  brief='Lists all off-duty users',
					  help='Lists all the users I know of in this server who\'ve not come back from being off-duty yet.')
	async def ListOffDutyUsers(self, ctx):
		# Get list of roles from database
		list = db_tools.ListUsers(ctx.guild.name)
		sendstring = 'Off-Duty Users:\n'
		for user in list:
			sendstring += f'{user[1]} - {user[3]} - Since {user[5]}\n'
		# Send message listing all roles
		await ctx.send(sendstring)

	@commands.command(name='test',
                      usage='test',
					  hidden=True,
					  brief='A testing command',
					  help='Does pretty much whatever I need it to, this is the test command for when I need to make '
						   'random little ideas actually work.')
	async def Test(self, ctx):
		send_string = f'Good test! Next test?'

		await ctx.send(send_string)


def setup(bot):
	bot.add_cog(Core(bot))