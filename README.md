# OffDutyBot

A bot to help Discord server leadership temporarily step away more effectively.

## Major TODOs

- Add Slash Command support
- Modify addRole to support selective off-duty
- Add custom prefix support
- Add confirmation to purgeServer
- Make prefix case-insensitive

## Permissions
- Manage Roles
- View Channels
- Send Messages
- Embed Links
- Read Message History
- Add Reactions (Unused right now, but planned.)

## Dependencies

- Discord.py
- python-dotenv

## Setup

### Self-Hosting

- Fork and download the repository.
- Update `sierra.ini`.
	- You don't *have* to change anything here, though it's recommended that you change `Owner` to the name of the user maintaining your instance of the bot.
	- It is recommended you do *not* change `Version` or `Repo` unless you are actively changing the code, though. It won't break anything, just make it less clear if your version of the bot is still even with mine.
- Copy and rename (Or just rename) `template.env` to `.env` and add the Token for your Discord bot (If you don't know how to create a bot on Discord's Developer portal, check out [this page](https://realpython.com/how-to-make-a-discord-bot-python/)).
- Create a link for your bot with the permissions listed above, and add it to your server.
- Run the Bot

### After Successfully Connected to a Server

Optionally, run `od;adminTutorial`. It'll go over everything here.

- Set up some leadership roles using `od;addRole <role>`
- Set up the off-duty role using `od;setOff-Duty <role>`
	- Create an @off-duty role if you don't have it yet (It doesn't need to literally be @off-duty, you can use whatever you'd like)
- Users can now set themselves off-duty with `od;off-duty`!

### Notes

This bot must be above every role in your server that you want it to be able to manage in order to add and remove roles from them. Typically, this means the bot will be at the very top of the permissions list.

## Commands

### General User Commands

- `off-duty` - Brings the user off-duty, removing the roles that have been set as leadership roles, and grants the user the off-duty role if applicable.
- `on-duty` - Brings the user back on-duty, removing the off-duty role and adding their existing leadership roles.
- `help` - Shows a help embed that lays out all commands.
- `help <command>` - Shows a more detailed explanation for a given command.
- `tutorial` - Shows an embed that explains the off-and on-duty commands.
- `adminTutorial` - Shows an embed for how to set up the bot. This command is available to all users, but most of the commands mentioned therein are restricted to admin.
- `listRoles` - Lists all roles in the server that are marked as leadership roles.
- `listUsers` - Lists all users who are currently recorded as off-duty.
- `ping` - Pings the bot.

### Admin Commands

Currently limited to users with the Admin role on the server

- `addRole <role>` - Adds the given role to the server's list of leadership roles.
- `setOffDuty <role>` - Sets the general off-duty role for the server.
- `remove <user>` - Removes any records for the given user. Useful if a user is removed from leadership for being off-duty too long, for example.
- `removeRole <roles>` - Removes the listed roles from the list of leadership roles, if found.
- `purgeServer` - Clears *all* information about the server from the database. Does not currently display a confirmation dialog, this is planned.